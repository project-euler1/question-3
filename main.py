def prime_factors(n):
    factors = [-1]
    d = 2
    while n > 1:
        while n % d == 0:
            factors.append(d)
            n /= d
        d += 1 if d == 2 else 2
        if d*d > n:
            if n > 1: factors.append(n)
            break
    return factors

x = prime_factors(600851475143)
print(int(x[len(x)-1]))